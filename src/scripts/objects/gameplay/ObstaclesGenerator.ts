import { settings } from "../../../config/settings"

const obstaclesKeys = ['triangle', 'circle', 'rectangle']
const screenHeight = [100, 360, 620]

export default class ObstaclesGenerator extends Phaser.GameObjects.GameObjectCreator {
    
    private obstacles: Phaser.GameObjects.Sprite[]
    private timer: number
    private positionsY: number[]

    constructor(scene: Phaser.Scene) {
        super(scene)

        this.obstacles = []
        this.timer = settings.gameplay.obstacle.TIMER
        this.positionsY = screenHeight.slice()
        
        obstaclesKeys.forEach(key => {
            this.addObstacle(key)
        });
    }

    private addObstacle(key: string) {
        const obstacle = this.sprite({key: key}, true)

        obstacle.setDisplaySize(settings.gameplay.obstacle.SIZE, settings.gameplay.obstacle.SIZE)
        obstacle.setData('figureKey', key)
        obstacle.x = this.scene.cameras.main.width
        obstacle.y = this.getPositionY()

        this.obstacles.push(obstacle)
    }

    private getPositionY() {
        const rndIndex = Math.floor(Math.random()*this.positionsY.length)
        const posY = this.positionsY[rndIndex]

        this.positionsY.splice(rndIndex, 1)

        if (this.positionsY.length === 0) {
            this.positionsY = screenHeight.slice()
        }

        return posY
    }

    private spawnObstacles() {
        // console.log('spawnObstacles')
        obstaclesKeys.forEach(key => {
            let created = false
            this.obstacles.forEach(obstacle => {
                if (obstacle.data.list.figureKey === key) {
                    if (!obstacle.visible && !obstacle.active) {
                        obstacle.x = this.scene.cameras.main.width
                        obstacle.y = this.getPositionY()
                        obstacle.setVisible(true)
                        obstacle.setActive(true)
                        created = true
                    }
                }
            });

            if (!created) {
                this.addObstacle(key)
            }
        });
    }

    public getObstacles() {
        return this.obstacles.filter(obs => {
            return obs.active && obs.visible
        })
    }

    public update() {
        this.obstacles.forEach(obs => {
            if (obs.active && obs.visible) {
                obs.x -= settings.gameplay.obstacle.SPEED_X
                if (obs.x + obs.displayWidth/2 < 0) {
                    obs.setVisible(false)
                    obs.setActive(false)
                }
            }
        });

        if (this.timer > 0) {
            this.timer--
        } else {
            this.timer = settings.gameplay.obstacle.TIMER
            this.spawnObstacles()
        }
    }
}