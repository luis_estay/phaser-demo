const KEY_OBJECT = 'triangle'

export default class ObjectsPool extends Phaser.GameObjects.Group {
    constructor(scene: Phaser.Scene, config: Phaser.Types.GameObjects.Group.GroupConfig = {}) {
        
		const defaults: Phaser.Types.GameObjects.Group.GroupConfig = {
			classType: Phaser.GameObjects.Image,
			maxSize: -1
		}

		super(scene, Object.assign(defaults, config))
	}

	spawn(x = 0, y = 0, key: string = KEY_OBJECT) {
		const gameObject: Phaser.GameObjects.Image = this.get(x, y, key)

		gameObject.setVisible(true)
		gameObject.setActive(true)

		return gameObject
    }

	despawn(gameObject: Phaser.GameObjects.Image) {
		this.killAndHide(gameObject)

		gameObject.alpha = 1
		gameObject.scale = 1
	}
}

/* Phaser.GameObjects.GameObjectFactory.register('cratePool', function () {
	// @ts-ignore
	return this.updateList.add(new CratePool(this.scene));
}) */