import { settings } from "../../../config/settings"

const screenHeight = [100, 360, 620]

export default class Player extends Phaser.GameObjects.Sprite {

    keys: any
    speed: number
    tweenHit: Phaser.Tweens.Tween

    constructor(scene: Phaser.Scene, x: number, y: number, key: string) {
        super(scene, x, y, key)

        this.setDisplaySize(settings.gameplay.player.SIZE, settings.gameplay.player.SIZE)
        this.setPosition(x + this.displayWidth/2, y)

        this.keys = this.scene.input.keyboard.addKeys('LEFT,RIGHT,UP,DOWN');

        this.tweenHit = this.scene.tweens.create({
            targets: this,
            paused: true,
            duration: 100,
            ease: 'Linear',
            completeDelay: 0,
            alpha: { start: 0, from: 0.5, to: 1 },
            repeat: 2
        })
    }

    public hit() {
        this.tweenHit.play()
    }

    public update() {
        if (this.keys.UP.isDown && this.y > screenHeight[0]/* this.y - this.displayHeight/2 > 0 */) 
            this.y -= settings.gameplay.player.SPEED_Y
        if (this.keys.DOWN.isDown && this.y < screenHeight[2] /* this.y + this.displayHeight/2 < this.scene.cameras.main.height */) 
            this.y += settings.gameplay.player.SPEED_Y
    }
}