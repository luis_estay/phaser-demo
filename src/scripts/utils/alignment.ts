export function centerGraphic(scene: Phaser.Scene, object: any, width: number, height: number, reference?: any) {
    if (reference) {
        //TODO: con referencia
    } else {
        object.x = scene.cameras.main.width / 2 - width / 2
        object.y = scene.cameras.main.height / 2 - height / 2
    }
}

export function center(scene: Phaser.Scene, object: any, reference?: any) {
    object.setOrigin(0.5, 0.5)
    if (reference) {
        //TODO: con referencia
    } else {
        object.x = scene.cameras.main.width / 2
        object.y = scene.cameras.main.height / 2
    }
}

export function centerX(scene: Phaser.Scene, object: any, reference?: any) {
    object.setOrigin(0.5, 0.5)
    if (reference) {
        //TODO: con referencia
    } else {
        object.x = scene.cameras.main.width / 2
    }
}

export function centerY(reference?: any) {}

export function alignLeft(reference?: any) {}

export function alignRight(reference?: any) {}

export function alignBottom(reference?: any) {}

export function alignTop(reference?: any) {}
