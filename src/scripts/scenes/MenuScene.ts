import PhaserLogo from "../objects/menu/phaserLogo"
import FpsText from "../objects/menu/fpsText"
import { BaseButton } from "../common/ui/baseButton"

export default class MenuScene extends Phaser.Scene {
    fpsText
    btn: BaseButton
    //frame: Phaser.GameObjects.Zone

    constructor() {
        super({ key: "MenuScene" })
    }

    create() {
        new PhaserLogo(this, this.cameras.main.width / 2, 0)
        this.fpsText = new FpsText(this)

        // display the Phaser.VERSION
        this.add
            .text(this.cameras.main.width - 15, 15, `Phaser v${Phaser.VERSION}`, {
                color: "#000000",
                fontSize: "24px"
            })
            .setOrigin(1, 0)
        this.btn = new BaseButton(
            this,
            this.cameras.main.width / 2,
            this.cameras.main.height / 2,
            "boton",
            () => this.testClick(),
            "Start"
        )
    }

    update() {
        this.fpsText.update()
    }

    testClick() {
        console.log("un click")
        this.scene.start("GameplayScene")
    }
}
