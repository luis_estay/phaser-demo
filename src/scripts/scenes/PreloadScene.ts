import "../utils/alignment"
import { center, centerGraphic } from "../utils/alignment"

export default class PreloadScene extends Phaser.Scene {
    progressBar: any
    progressBox: any
    loadingText: any
    percentText: any
    logo: any
    constructor() {
        super({ key: "PreloadScene" })
    }

    preload() {
        this.load.image("phaser-logo", "assets/images/phaser-logo.png")
        this.setLoadBar()
    }

    create() {
        this.scene.start("MenuScene")
        /**
         * This is how you would dynamically import the mainScene class (with code splitting),
         * add the mainScene to the Scene Manager
         * and start the scene.
         * The name of the chunk would be 'mainScene.chunk.js
         * Find more about code splitting here: https://webpack.js.org/guides/code-splitting/
         */
        // let someCondition = true
        // if (someCondition)
        //   import(/* webpackChunkName: "mainScene" */ './mainScene').then(mainScene => {
        //     this.scene.add('MainScene', mainScene.default, true)
        //   })
        // else console.log('The mainScene class will not even be loaded by the browser')
    }

    setLoadBar() {
        let width = this.cameras.main.width
        let height = this.cameras.main.height

        this.progressBar = this.add.graphics()
        this.progressBox = this.add.graphics()
        this.progressBox.fillStyle(0x222222, 0.8)
        //this.progressBox.fillRect((width - 320) / 2, (height - 50) / 2, 320, 50)
        this.progressBox.fillRect(0, 0, 320, 50)
        centerGraphic(this, this.progressBox, 320, 50)

        this.loadingText = this.make.text({
            x: width / 2,
            y: height / 2 - 50,
            text: "Cargando...",
            style: {
                font: "20px monospace",
                color: "#ffffff"
            }
        })
        this.loadingText.setOrigin(0.5, 0.5)

        this.percentText = this.make.text({
            x: width / 2,
            y: height / 2,
            text: "0%",
            style: {
                font: "18px monospace",
                color: "#ffffff"
            }
        })
        this.percentText.setOrigin(0.5, 0.5)

        this.load.on("progress", (value: any) => {
            this.percentText.setText(parseInt(value * 100 + "") + "%")
            this.progressBar.clear()
            this.progressBar.fillStyle(0xffffff, 1)
            this.progressBar.fillRect(this.progressBox.x + 10, this.progressBox.y + 10, 300 * value, 30)
        })
        this.load.on("complete", () => {
            this.progressBar.destroy()
            this.progressBox.destroy()
            this.loadingText.destroy()
            this.percentText.destroy()
        })
        this.loadAssets()
    }

    loadAssets() {
        this.load.image("logo", "assets/images/phaser3-logo.png")
        this.load.spritesheet("boton", "assets/sprites/boton.png", { frameWidth: 250, frameHeight: 80 })
        // this.load.spritesheet("panel", "assets/sprites/panel.png", { frameWidth: 109, frameHeight: 109 })
        for (var i = 0; i < 1000; i++) {
            this.load.image("logo" + i, "assets/images/phaser3-logo.png")
        }
    }
}
