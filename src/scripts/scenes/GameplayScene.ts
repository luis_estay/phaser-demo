import ObjectsPool from "../objects/gameplay/ObjectsPool";
import ObstaclesGenerator from "../objects/gameplay/ObstaclesGenerator";
import Player from "../objects/gameplay/Player";

export default class GameplayScene extends Phaser.Scene {

    player: Player
    obstacles: ObjectsPool
    obstaclesGenerator: ObstaclesGenerator
    background: Phaser.GameObjects.TileSprite
    textScore: Phaser.GameObjects.Text
    score: number

    constructor() {
        super({ key: "GameplayScene" })
        this.score = 0
    }

    preload() {
        /* Background */
        this.load.image('bg', 'assets/sprites/bg/background.png')
        this.load.image('bg1', 'assets/sprites/bg/sky1.png')
        // this.load.image('bg2', 'assets/sprites/bg/sky2.png')
        this.load.image('bg3', 'assets/sprites/bg/sky3.png')
        // this.load.image('bg4', 'assets/sprites/bg/sky4.png')
        this.load.image('moon', 'assets/sprites/bg/moon.png')
        this.load.image('stars', 'assets/sprites/bg/stars.png')

        /* Player */
        this.load.image('player', 'assets/sprites/circle.png')

        /* Obstacles */
        this.load.image('triangle', 'assets/sprites/triangle.png')
        this.load.image('rectangle', 'assets/sprites/rectangle.png')
        this.load.image('circle', 'assets/sprites/circle.png')
    }

    create() {
        this.player = new Player(this, 50, this.cameras.main.height/2, 'player')

        // this.add.image(this.cameras.main.width/2, this.cameras.main.height/2, 'bg')
        this.add.image(this.cameras.main.width/2, this.cameras.main.height/2, 'bg1')
        // this.add.image(this.cameras.main.width/2, this.cameras.main.height/2, 'bg3')
        // this.add.image(this.cameras.main.width/2, this.cameras.main.height/2, 'stars')
        // this.add.image(this.cameras.main.width/2, this.cameras.main.height/2, 'moon')
        this.background = this.add.tileSprite(this.cameras.main.width/2, this.cameras.main.height/2, this.cameras.main.width, this.cameras.main.height, 'stars');
        this.add.existing(this.player)
        this.obstaclesGenerator = new ObstaclesGenerator(this)

        this.textScore = this.add.text(50, 20, '', { font: '24px Courier'});
        this.textScore.setText([
            'Score: ' + this.score
        ]);
    }

    update() {
        this.player.update()
        this.obstaclesGenerator.update()
        this.background.tilePositionX += 1

        this.checkCollision()
    }

    checkCollision() {
        const obstacles = this.obstaclesGenerator.getObstacles()

        obstacles.forEach(obstacle => {
            if (obstacle.x < 200) {
                const collision = Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), obstacle.getBounds())
                if (collision) {
                    if (obstacle.data.list.figureKey === 'circle') {
                        obstacle.setVisible(false)
                        obstacle.setActive(false)
                        this.score += 100
                        this.textScore.setText([
                            'Score: ' + this.score
                        ]);
                    } else {
                        this.player.hit()
                    }
                }
            }
        });
    }
}