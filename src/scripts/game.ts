import "phaser"
import MenuScene from "./scenes/MenuScene"
import PreloadScene from "./scenes/PreloadScene"
import GameplayScene from "./scenes/GameplayScene"

const DEFAULT_WIDTH = 1280
const DEFAULT_HEIGHT = 720

const config = {
    type: Phaser.AUTO,
    backgroundColor: "#BAC5CC",
    scale: {
        parent: "phaser-game",
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: DEFAULT_WIDTH,
        height: DEFAULT_HEIGHT
    },
    scene: [
        PreloadScene, 
        MenuScene,
        GameplayScene,
    ],
    physics: {
        default: "arcade",
        arcade: {
            debug: false,
            gravity: { y: 400 }
        }
    }
}

window.addEventListener("load", () => {
    const game = new Phaser.Game(config)
})
