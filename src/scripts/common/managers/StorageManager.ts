export class StorageManager {
    
    private prefix: string

    constructor(prefix: string = 'ogr_') {
        this.prefix = prefix
    }

    set(key: string, value: string) {
        localStorage.setItem(this.prefix + key, value)
    }

    get(key: string) {
        return localStorage.getItem(this.prefix + key);
    }

    remove(key: string) {
        localStorage.removeItem(this.prefix + key)
    }

    clear() {
        localStorage.clear()
    }
}