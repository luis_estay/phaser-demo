export class BaseButton extends Phaser.GameObjects.Container {
    text: Phaser.GameObjects.Text
    images: Phaser.GameObjects.Sprite

    constructor(
        scene: Phaser.Scene,
        x: number,
        y: number,
        spritesheet: string,
        callback?: any,
        text?: string,
        textStyle?: any,
        children?: any
    ) {
        super(scene, x, y, children)
        scene.add.existing(this)

        this.images = scene.add.sprite(0, 0, spritesheet, 0)
        this.add(this.images)
        if (text) {
            this.text = scene.make.text({
                x: 0,
                y: 0,
                text: text,
                style: textStyle
                    ? textStyle
                    : {
                          fontFamily: "Courier",
                          fontSize: "24px",
                          color: "#ffffff"
                      }
            })
            this.text.setOrigin(0.5, 0.5)
            this.add(this.text)
        }

        this.images
            .setInteractive({ useHandCursor: true })
            .on("pointerover", () => this.enterButtonHoverState())
            .on("pointerout", () => this.enterButtonRestState())
            .on("pointerdown", () => this.enterButtonActiveState())
            .on("pointerup", () => {
                this.enterButtonHoverState()
                if (callback) callback()
            })
    }

    enterButtonHoverState() {
        //this.setStyle({ fill: "#ff0 " })
        this.images.setFrame(1)
    }

    enterButtonRestState() {
        //this.setStyle({ fill: "#0f0 " })
        this.images.setFrame(0)
    }

    enterButtonActiveState() {
        //this.setStyle({ fill: "#0ff" })
    }
}
