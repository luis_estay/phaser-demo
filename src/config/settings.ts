export const settings = {
    menu: {},
    ui: {},
    gameplay: {
        timer: 20,
        player: {
            SPEED_Y: 12,
            SIZE: 150
        },
        obstacle: {
            SIZE: 150,
            SPEED_X: 10,
            TIMER: 60 //100
        },
        background: {
            SPEED_X: 5
        }
    }
}